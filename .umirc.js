
// ref: https://umijs.org/config/
export default {
  dva: {
    immer: true
  },
  title: 'react-umi',
}
