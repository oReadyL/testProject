module.exports = {
	presets: [
		[
			'@babel/env',
			{
				modules: 'commonjs',
				useBuiltIns: 'usage',
				corejs: 3,
			},
		],
		'@babel/react',
	]
};
