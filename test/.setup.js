require("ignore-styles");
require("mocha-chai-snapshots");
require("@babel/register")(require("../babel.config"));

var enzyme = require('enzyme');
var Adapter = require('enzyme-adapter-react-16');
var Blob= require("cross-blob");
enzyme.configure({ adapter: new Adapter() });

new Blob([]);
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const { window } = new JSDOM().window;
const{ document, HTMLElement, File } = window;

global.window = window;
global.document = document;
global.HTMLElement = HTMLElement;
global.File = File;
global.navigator = {userAgent: 'node.js'};
global.Blob = Blob;
