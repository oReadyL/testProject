import React from "react";
import schema from "./schema.json";
import FormRender from "form-render/lib/antd";
import styles from "./style.less";

export default class Test extends React.PureComponent {
    constructor(){
        super();
        this.state = {
            propsSchema: {},
            uiSchema: {},
            formData: {}
        }
    }

    componentDidMount(){
        setTimeout(() => {
            this.setState({
                propsSchema: schema.propsSchema,
                uiSchema: schema.uiSchema
            })
        }, 3000);
    }
    
    handleChange = formData => {
        this.setState({
            formData
        })
    }

    render() {
        const { propsSchema, uiSchema, formData} = this.state;
        return (
            <div className={styles.wrapper}>
                <FormRender
                    displayType="row"
                    showDescIcon
                    propsSchema={propsSchema}
                    uiSchema={uiSchema}
                    formData={formData}
                    onChange={this.handleChange}
                />
            </div>
        )
    }
}