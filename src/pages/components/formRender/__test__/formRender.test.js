import React from "react";
import FormRender from "../index";
import { expect } from "chai";
import { mount } from "enzyme";

describe("Test", () => {
    let comp;
    beforeEach(() => {
        comp = mount(<FormRender />)        
    });
    it("存在", () => {
        console.log(comp.html());
        expect(comp.find(".fr-label-title").length).equal(5)
    })
})
