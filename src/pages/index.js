import React, { Component, useEffect, useState, useCallback } from 'react';
import FormRender from './components/formRender';
import { Table } from 'antd';
import request from 'umi-request';

export default () => {
  const [pageNo, setPageNo] = useState(1);
  const [data, setData] = useState([]);

  useEffect(() => {
    getListData();
  }, [getListData]);

  const getListData = useCallback(
    (pageNo) => {
      request.get(`/list/songs?pageNo=${pageNo}`).then((res) => {
        setData(res.list);
      });
    },
    [pageNo],
  );

  const onChange = (pageNo, pageSize) => {
    setPageNo(pageNo);
    getListData(pageNo);
  };
  const onShowSizeChange = (pageNo) => {
    pageNo(pageNo);
    getListData(pageNo);
  };
  return (
    <Table
      dataSource={data}
      columns={[
        {
          title: '序号',
          label: '序号',
          dataIndex: 'id',
        },
      ]}
      rowKey="id"
      pagination={{
        size: 'small',
        total: 100,
        pageSize: 10,
        current: pageNo,
        showSizeChanger: true,
        showQuickJumper: true,
        showTotal: (total) => `Total ${total} items`,
        onChange: onChange,
        onShowSizeChange: onShowSizeChange,
      }}
    />
  );
};
