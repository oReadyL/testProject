import Mock from 'mockjs'; //mockjs
import { delay } from 'roadhog-api-doc'; //模拟服务器响应延时
const { Random } = Mock;

let data = Mock.mock({
  natral: Random.natural(10, 20),
});

const proxy = {
  'GET /admin/isLogin': Mock.mock({ isLogin: Random.boolean() }),
  'GET /list/songs': (req, res) => {
    const {
      query: { pageNo, pageSize },
    } = req;
    let list = [];
    if (String(pageNo) === '3') {
      list = Mock.mock({
        'list|12': [
          {
            'id|+1': 1,
          },
        ],
      });
    } else {
      list = Mock.mock({
        'list|10': [
          {
            'id|+1': 1,
          },
        ],
      });
    }
    res.send(list);
  },
};

export default delay(proxy, Random.natural(1000, 1000));
